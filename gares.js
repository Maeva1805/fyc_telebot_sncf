const gares = [
    {
        name: "Montparnasse",
        id: "stop_area:SNCF:87391003"
    },
    {
        name: "Paris Est",
        id: "stop_area:SNCF:87113001"
    },
    {
        name: "Paris Nord",
        id: "stop_area:SNCF:87271007"
    },
    {
        name: "Gare de Lyon",
        id: "stop_area:SNCF:87686006"
    },
    {
        name: "Saint Lazare",
        id: "stop_area:SNCF:87384008"
    },
    {
        name: "Achères Ville",
        id: "stop_area:SNCF:87381657"
    },
    {
        name: "Mantes-la-Jolie",
        id: "stop_area:SNCF:87381509"
    }
];

module.exports = { gares };