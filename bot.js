const { Telegraf } = require('telegraf');
const fetch = require('node-fetch');
let { gares } = require('./gares')

const BOT_TOKEN = "5056206863:AAFGdWDubYzGtilgNVFB-ZTKrCgAYBNJJcQ";
const API_SNCF_TOKEN = "6bf862b3-1cf9-4bea-b4ba-947aa14d1469";
const API_SNCF_URL = `https://${API_SNCF_TOKEN}@api.sncf.com/v1`;

const bot = new Telegraf(BOT_TOKEN);

bot.start((ctx) => {
    let msg = `Bonjour ! \nVoici la liste des commandes que vous pouvez utiliser :

/departs nomDeLaGare
pour afficher les prochains départs.

/arrivees nomDeLaGare
pour afficher les prochaines arrivées.

/itineraire gare1 gare2
pour trouver le meilleur itinéraire en partant tout de suite.

Si le nom de la gare a un espace, remplacez le par _

Voici les gares actuellement disponibles :\n`;
    gares.forEach(g => msg += `\n- ${g.name}`);
    ctx.reply(msg);
});

bot.command('departs', async (ctx) => {
    ctx.reply('Recherche des prochains départs...');
    const ville = ctx.update.message.text.split(' ')[1];

    const gareData = ville ? gares.find(g => g.name === ville.replace("_", " ")) : undefined;

    if(gareData) {
        await fetch(`${API_SNCF_URL}/coverage/sncf/stop_areas/${gareData.id}/departures?datetime=${new Date().toJSON()}`)
        .then(res => res.json())
        .then(json => {
            let msg = `Prochains départs de ${gareData.name} :`;
            const departs = json.departures;
            departs.forEach(d => {
                const infos = d.display_informations;
                // name
                const dir = infos.direction;
                // ligne
                const ligne = infos.code !== "" ? infos.code : infos.commercial_mode;

                // datetime
                const timeStr = d.stop_date_time.departure_date_time;
                // convert to date with valid UTC string
                const time = new Date(
                    `${timeStr.slice(0, 4)}-${timeStr.slice(4, 6)}-${timeStr.slice(6, 8)}T${timeStr.slice(9, 11)}:${timeStr.slice(11, 13)}:${timeStr.slice(13, 15)}`
                );
                const hours = String(time.getHours()).padStart(2, '0');
                const minutes = String(time.getMinutes()).padStart(2, '0');

                msg += `\n\n[ ${ligne} ] ${dir}\nDépart à ${hours}:${minutes}`;
            });
            ctx.reply(msg);
        })
        .catch(err => console.log("ERROR : ", err));
    } else {
        ctx.reply("La gare que vous cherchez n'est pas disponible. Vérifiez que vous avez bien écrit le nom et que vous avez remplacé les espace par _");
    }

    
});

bot.command('arrivees', async (ctx) => {
    ctx.reply('Recherche des prochaines arrivées...');
    const ville = ctx.update.message.text.split(' ')[1];

    const gareData = ville ? gares.find(g => g.name === ville.replace("_", " ")) : undefined;

    if(gareData) {
        await fetch(`${API_SNCF_URL}/coverage/sncf/stop_areas/${gareData.id}/arrivals?datetime=${new Date().toJSON()}`)
        .then(res => res.json())
        .then(json => {
            let msg = `Prochaines arrivées à ${gareData.name} :`;
            const arrivees = json.arrivals;
            arrivees.forEach(d => {
                const infos = d.display_informations;
                // name
                const dir = infos.direction;
                // ligne
                const ligne = infos.code !== "" ? infos.code : infos.commercial_mode;

                // datetime
                const timeStr = d.stop_date_time.arrival_date_time;
                // convert to date with valid UTC string
                const time = new Date(
                    `${timeStr.slice(0, 4)}-${timeStr.slice(4, 6)}-${timeStr.slice(6, 8)}T${timeStr.slice(9, 11)}:${timeStr.slice(11, 13)}:${timeStr.slice(13, 15)}`
                );
                const hours = String(time.getHours()).padStart(2, '0');
                const minutes = String(time.getMinutes()).padStart(2, '0');

                msg += `\n\n[ ${ligne} ] ${dir}\nArrivée à ${hours}:${minutes}`;
            });
            ctx.reply(msg);
        })
        .catch(err => console.log("ERROR : ", err));
    } else {
        ctx.reply("La gare que vous cherchez n'est pas disponible. Vérifiez que vous avez bien écrit le nom et que vous avez remplacé les espace par _");
    }

    
});

bot.command('itineraire', async (ctx) => {
    const ville1 = ctx.update.message.text.split(' ')[1];
    const ville2 = ctx.update.message.text.split(' ')[2];

    const gare1 = ville1 ? gares.find(g => g.name === ville1.replace("_", " ")) : undefined;
    const gare2 = ville2 ? gares.find(g => g.name === ville2.replace("_", " ")) : undefined;

    ctx.reply(`Recherche du meilleur itinéraire entre ${gare1.name} et ${gare2.name}...`);

    if(gare1 && gare2) {
        await fetch(`${API_SNCF_URL}/coverage/sncf/journeys?from=${gare1.id}&to=${gare2.id}&datetime=${new Date().toJSON()}`)
        .then(res => res.json())
        .then(json => {
            let msg = `Meilleur itinéraire de ${gare1.name} à ${gare2.name} :`;
            const journey = json.journeys.find(j => j.type === "best");
            const totalDuration = secondsToFormat(journey.duration);
            msg += `\n(${totalDuration})`;

            journey.sections.forEach(d => {
                if(d.duration > 0) {
                    if(d.type === "walking") {
                        msg += `\n\n${secondsToFormat(d.duration)} de marche`;
                    } else if(d.type === "waiting") {
                        msg += `\n\n${secondsToFormat(d.duration)} d'attente`;
                    } else if(d.type === "public_transport") {
                        const from = d.from.name;
                        const to = d.to.name;

                        const departStr = d.departure_date_time;
                        // convert to date with valid UTC string
                        const timeDepart = new Date(
                            `${departStr.slice(0, 4)}-${departStr.slice(4, 6)}-${departStr.slice(6, 8)}T${departStr.slice(9, 11)}:${departStr.slice(11, 13)}:${departStr.slice(13, 15)}`
                        );
                        const hoursDepart = String(timeDepart.getHours()).padStart(2, '0');
                        const minutesDepart = String(timeDepart.getMinutes()).padStart(2, '0');

                        const arriveeStr = d.arrival_date_time;
                        // convert to date with valid UTC string
                        const timeArrivee = new Date(
                            `${arriveeStr.slice(0, 4)}-${arriveeStr.slice(4, 6)}-${arriveeStr.slice(6, 8)}T${arriveeStr.slice(9, 11)}:${arriveeStr.slice(11, 13)}:${arriveeStr.slice(13, 15)}`
                        );
                        const hoursArrivee = String(timeArrivee.getHours()).padStart(2, '0');
                        const minutesArrivee = String(timeArrivee.getMinutes()).padStart(2, '0');

                        msg += `\n\n${hoursDepart}h${minutesDepart} ${from}\n->\n${hoursArrivee}h${minutesArrivee} ${to}`;

                        const infos = d.display_informations;
                        // name
                        const dir = infos.direction;
                        // ligne
                        const ligne = infos.code !== "" ? infos.code : infos.commercial_mode;

                        msg += `\n[ ${ligne} ] ${dir}`;
                    }
                }
            });
            ctx.reply(msg);
        })
        .catch(err => console.log("ERROR : ", err));
    } else {
        ctx.reply("La gare que vous cherchez n'est pas disponible. Vérifiez que vous avez bien écrit le nom et que vous avez remplacé les espace par _");
    }

    
});

const secondsToFormat = (nbSec) => {
    let date = new Date(null);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(nbSec);
    return `${date.getHours() > 0 ? `${String(date.getHours()).padStart(2, '0')}h` : ""}${String(date.getMinutes()).padStart(2, '0')}${date.getHours() === 0 ? "min" : ""}`;
}

bot.launch();